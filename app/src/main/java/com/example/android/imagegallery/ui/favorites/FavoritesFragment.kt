package com.example.android.imagegallery.ui.favorites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.android.imagegallery.R
import com.example.android.imagegallery.databinding.FragmentFavoritesBinding
import com.example.android.imagegallery.domain.ImageEntity
import com.example.android.imagegallery.viewmodels.favorites.FavouriteViewModel

class FavoritesFragment : Fragment() {

    private val viewModel: FavouriteViewModel by lazy {
        val activity = requireNotNull(this.activity) {
            getString(R.string.text_error_viewmodel_early_access)
        }
        ViewModelProvider(this, FavouriteViewModel.Factory(activity.application))
                .get(FavouriteViewModel::class.java)
    }

    private var viewModelAdapter: FavoritesAdapter? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {

        val binding: FragmentFavoritesBinding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_favorites,
                container,
                false)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        viewModelAdapter = FavoritesAdapter(
                FavoritesAdapter.OnClickListener { viewModel.displayImageDetails(it) },
                FavoritesAdapter.OnClickListener { handleFavoriteDelete(it) })

        viewModel.navigateToSelectedImage.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                this.findNavController().navigate(
                        FavoritesFragmentDirections.actionShowDetail(it))
                viewModel.displayImageDetails(it)
                viewModel.displayImageDetailsNullifyTarget()
            }
        })

        val recycler = binding.root.findViewById<RecyclerView>(R.id.recycler_view)
        recycler.adapter = viewModelAdapter
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.images.observe(viewLifecycleOwner, Observer<List<ImageEntity>> { img ->
            img?.apply {
                viewModelAdapter?.images = img
            }
        })
    }

    private fun handleFavoriteDelete(image: ImageEntity) {
        viewModel.deleteFavorite(image)
        Toast.makeText(context, getString(R.string.toast_message_favorite_delete_success), Toast.LENGTH_SHORT).show()
    }
}
package com.example.android.imagegallery.databinding;
import com.example.android.imagegallery.R;
import com.example.android.imagegallery.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentDetailBindingImpl extends FragmentDetailBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.constraintLayout_top, 4);
        sViewsWithIds.put(R.id.constraintLayout_bottom, 5);
        sViewsWithIds.put(R.id.button_browser, 6);
        sViewsWithIds.put(R.id.button_save, 7);
        sViewsWithIds.put(R.id.button_wallpaper, 8);
    }
    // views
    @NonNull
    private final android.widget.ScrollView mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentDetailBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 9, sIncludes, sViewsWithIds));
    }
    private FragmentDetailBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (android.widget.Button) bindings[6]
            , (android.widget.Button) bindings[7]
            , (android.widget.Button) bindings[8]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[5]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[4]
            , (android.widget.TextView) bindings[3]
            , (android.widget.ImageView) bindings[1]
            , (android.widget.TextView) bindings[2]
            );
        this.itemAuthor.setTag(null);
        this.itemImage.setTag(null);
        this.itemSize.setTag(null);
        this.mboundView0 = (android.widget.ScrollView) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.image == variableId) {
            setImage((com.example.android.imagegallery.viewmodels.detail.DetailViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setImage(@Nullable com.example.android.imagegallery.viewmodels.detail.DetailViewModel Image) {
        this.mImage = Image;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.image);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeImageSelectedImage((androidx.lifecycle.LiveData<com.example.android.imagegallery.domain.ImageEntity>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeImageSelectedImage(androidx.lifecycle.LiveData<com.example.android.imagegallery.domain.ImageEntity> ImageSelectedImage, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.example.android.imagegallery.viewmodels.detail.DetailViewModel image = mImage;
        com.example.android.imagegallery.domain.ImageEntity imageSelectedImageGetValue = null;
        java.lang.String imageSelectedImageDownloadUrl = null;
        java.lang.String imageSelectedImageSize = null;
        java.lang.String imageSelectedImageAuthor = null;
        androidx.lifecycle.LiveData<com.example.android.imagegallery.domain.ImageEntity> imageSelectedImage = null;

        if ((dirtyFlags & 0x7L) != 0) {



                if (image != null) {
                    // read image.selectedImage
                    imageSelectedImage = image.getSelectedImage();
                }
                updateLiveDataRegistration(0, imageSelectedImage);


                if (imageSelectedImage != null) {
                    // read image.selectedImage.getValue()
                    imageSelectedImageGetValue = imageSelectedImage.getValue();
                }


                if (imageSelectedImageGetValue != null) {
                    // read image.selectedImage.getValue().download_url
                    imageSelectedImageDownloadUrl = imageSelectedImageGetValue.getDownload_url();
                    // read image.selectedImage.getValue().size
                    imageSelectedImageSize = imageSelectedImageGetValue.getSize();
                    // read image.selectedImage.getValue().author
                    imageSelectedImageAuthor = imageSelectedImageGetValue.getAuthor();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.itemAuthor, imageSelectedImageAuthor);
            com.example.android.imagegallery.util.BindingAdaptersKt.setImageUrl(this.itemImage, imageSelectedImageDownloadUrl);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.itemSize, imageSelectedImageSize);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): image.selectedImage
        flag 1 (0x2L): image
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}
package com.example.android.imagegallery.ui.detail;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavArgs;
import com.example.android.imagegallery.domain.ImageEntity;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class DetailFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private DetailFragmentArgs() {
  }

  private DetailFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static DetailFragmentArgs fromBundle(@NonNull Bundle bundle) {
    DetailFragmentArgs __result = new DetailFragmentArgs();
    bundle.setClassLoader(DetailFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("selectedImage")) {
      ImageEntity selectedImage;
      if (Parcelable.class.isAssignableFrom(ImageEntity.class) || Serializable.class.isAssignableFrom(ImageEntity.class)) {
        selectedImage = (ImageEntity) bundle.get("selectedImage");
      } else {
        throw new UnsupportedOperationException(ImageEntity.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      __result.arguments.put("selectedImage", selectedImage);
    } else {
      throw new IllegalArgumentException("Required argument \"selectedImage\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public ImageEntity getSelectedImage() {
    return (ImageEntity) arguments.get("selectedImage");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("selectedImage")) {
      ImageEntity selectedImage = (ImageEntity) arguments.get("selectedImage");
      if (Parcelable.class.isAssignableFrom(ImageEntity.class) || selectedImage == null) {
        __result.putParcelable("selectedImage", Parcelable.class.cast(selectedImage));
      } else if (Serializable.class.isAssignableFrom(ImageEntity.class)) {
        __result.putSerializable("selectedImage", Serializable.class.cast(selectedImage));
      } else {
        throw new UnsupportedOperationException(ImageEntity.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    DetailFragmentArgs that = (DetailFragmentArgs) object;
    if (arguments.containsKey("selectedImage") != that.arguments.containsKey("selectedImage")) {
      return false;
    }
    if (getSelectedImage() != null ? !getSelectedImage().equals(that.getSelectedImage()) : that.getSelectedImage() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getSelectedImage() != null ? getSelectedImage().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "DetailFragmentArgs{"
        + "selectedImage=" + getSelectedImage()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(DetailFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder(@Nullable ImageEntity selectedImage) {
      this.arguments.put("selectedImage", selectedImage);
    }

    @NonNull
    public DetailFragmentArgs build() {
      DetailFragmentArgs result = new DetailFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setSelectedImage(@Nullable ImageEntity selectedImage) {
      this.arguments.put("selectedImage", selectedImage);
      return this;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public ImageEntity getSelectedImage() {
      return (ImageEntity) arguments.get("selectedImage");
    }
  }
}

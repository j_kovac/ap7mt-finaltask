package com.example.android.imagegallery.ui.favorites;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \u00072\u00020\u0001:\u0001\u0007B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\b"}, d2 = {"Lcom/example/android/imagegallery/ui/favorites/ImageViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "viewDataBinding", "Lcom/example/android/imagegallery/databinding/FavoritesItemBinding;", "(Lcom/example/android/imagegallery/databinding/FavoritesItemBinding;)V", "getViewDataBinding", "()Lcom/example/android/imagegallery/databinding/FavoritesItemBinding;", "Companion", "app_debug"})
public final class ImageViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
    @org.jetbrains.annotations.NotNull()
    private final com.example.android.imagegallery.databinding.FavoritesItemBinding viewDataBinding = null;
    @androidx.annotation.LayoutRes()
    private static final int LAYOUT = com.example.android.imagegallery.R.layout.favorites_item;
    public static final com.example.android.imagegallery.ui.favorites.ImageViewHolder.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.android.imagegallery.databinding.FavoritesItemBinding getViewDataBinding() {
        return null;
    }
    
    public ImageViewHolder(@org.jetbrains.annotations.NotNull()
    com.example.android.imagegallery.databinding.FavoritesItemBinding viewDataBinding) {
        super(null);
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\u00020\u00048\u0006X\u0087D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/example/android/imagegallery/ui/favorites/ImageViewHolder$Companion;", "", "()V", "LAYOUT", "", "getLAYOUT", "()I", "app_debug"})
    public static final class Companion {
        
        public final int getLAYOUT() {
            return 0;
        }
        
        private Companion() {
            super();
        }
    }
}
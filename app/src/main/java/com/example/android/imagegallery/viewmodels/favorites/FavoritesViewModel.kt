package com.example.android.imagegallery.viewmodels.favorites

import android.app.Application
import androidx.lifecycle.*
import com.example.android.imagegallery.database.getDatabase
import com.example.android.imagegallery.domain.ImageEntity
import com.example.android.imagegallery.repository.ImagesRepository
import kotlinx.coroutines.launch
import java.io.IOException

class FavouriteViewModel(application: Application) : ViewModel() {

    private var _eventNetworkError = MutableLiveData<Boolean>(false)
    val eventNetworkError: LiveData<Boolean>
        get() = _eventNetworkError

    private val imageRepository = ImagesRepository(getDatabase(application, "lorempicsum"))
    val images = imageRepository.images

    private val _navigateToSelectedImage = MutableLiveData<ImageEntity>()
    val navigateToSelectedImage: LiveData<ImageEntity>
        get() = _navigateToSelectedImage


    init {
        getFavoriteImages()
    }

    private fun getFavoriteImages() = viewModelScope.launch {
        viewModelScope.launch {
            try {
                _eventNetworkError.value = false
                imageRepository.getImagesDB()
            } catch (networkError: IOException) {
            }
        }
    }

    fun deleteFavorite(image: ImageEntity) = viewModelScope.launch {
        viewModelScope.launch {
            try {
                _eventNetworkError.value = false
                imageRepository.removeImage(image.id)
                getFavoriteImages()
            } catch (networkError: IOException) {
            }
        }
    }

    fun displayImageDetails(image: ImageEntity?) {
        _navigateToSelectedImage.value = image
    }

    fun displayImageDetailsNullifyTarget() {
        _navigateToSelectedImage.value = null
    }

    class Factory(val app: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(FavouriteViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return FavouriteViewModel(app) as T
            }
            throw IllegalArgumentException("Unable to construct viewmodel")
        }
    }
}
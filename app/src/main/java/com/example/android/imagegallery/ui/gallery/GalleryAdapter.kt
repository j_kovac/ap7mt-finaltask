package com.example.android.imagegallery.ui.gallery

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.android.imagegallery.R
import com.example.android.imagegallery.databinding.ImagegalleryItemBinding
import com.example.android.imagegallery.domain.ImageEntity
import kotlinx.android.synthetic.main.imagegallery_item.view.*

class GalleryAdapter(
        private val onClickListener: OnClickListener,
        private val onScrollListener: OnScrollListener,
        private val onClickListenerFav: OnClickListener) : RecyclerView.Adapter<ImageViewHolder>() {

    var images: List<ImageEntity> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val withDataBinding: ImagegalleryItemBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                ImageViewHolder.LAYOUT,
                parent,
                false
        )
        return ImageViewHolder(withDataBinding)
    }

    override fun getItemCount() = images.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.viewDataBinding.also {
            val item = images[position]
            it.image = item
            holder.itemView.setOnClickListener {
                onClickListener.onClick(item)
            }
            holder.itemView.favorize_button.setOnClickListener {
                onClickListenerFav.onClick(item)
                holder.itemView.favorize_button.setImageResource(R.drawable.ic_favorite)
            }

            if (item.isFavorite) {
                holder.itemView.favorize_button.setImageResource(R.drawable.ic_favorite)
            } else {
                holder.itemView.favorize_button.setImageResource(R.drawable.ic_not_favorite)
            }
        }
        if (position == images.size - 1) {
            onScrollListener.onScroll()
        }
    }

    class OnClickListenerFav(val clickListener: (image: ImageEntity) -> Unit) {
        fun onClick(image: ImageEntity) = clickListener(image)
    }

    class OnClickListener(val clickListener: (image: ImageEntity) -> Unit) {
        fun onClick(image: ImageEntity) = clickListener(image)
    }

    class OnScrollListener(val scrollListener: () -> Unit) {
        fun onScroll() = scrollListener()
    }
}

class ImageViewHolder(val viewDataBinding: ImagegalleryItemBinding) : RecyclerView.ViewHolder(viewDataBinding.root) {
    companion object {
        @LayoutRes
        val LAYOUT = R.layout.imagegallery_item
    }
}
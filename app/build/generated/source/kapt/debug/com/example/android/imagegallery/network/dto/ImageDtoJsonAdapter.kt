// Code generated by moshi-kotlin-codegen. Do not edit.
package com.example.android.imagegallery.network.dto

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.Moshi
import com.squareup.moshi.internal.Util
import java.lang.NullPointerException
import kotlin.Int
import kotlin.String
import kotlin.Suppress
import kotlin.collections.emptySet
import kotlin.text.buildString

@Suppress("DEPRECATION", "unused", "ClassName", "REDUNDANT_PROJECTION")
class ImageDtoJsonAdapter(
  moshi: Moshi
) : JsonAdapter<ImageDto>() {
  private val options: JsonReader.Options = JsonReader.Options.of("id", "author", "width", "height",
      "url", "download_url")

  private val stringAdapter: JsonAdapter<String> = moshi.adapter(String::class.java, emptySet(),
      "id")

  private val intAdapter: JsonAdapter<Int> = moshi.adapter(Int::class.java, emptySet(), "width")

  override fun toString(): String = buildString(30) {
      append("GeneratedJsonAdapter(").append("ImageDto").append(')') }

  override fun fromJson(reader: JsonReader): ImageDto {
    var id: String? = null
    var author: String? = null
    var width: Int? = null
    var height: Int? = null
    var url: String? = null
    var download_url: String? = null
    reader.beginObject()
    while (reader.hasNext()) {
      when (reader.selectName(options)) {
        0 -> id = stringAdapter.fromJson(reader) ?: throw Util.unexpectedNull("id", "id", reader)
        1 -> author = stringAdapter.fromJson(reader) ?: throw Util.unexpectedNull("author",
            "author", reader)
        2 -> width = intAdapter.fromJson(reader) ?: throw Util.unexpectedNull("width", "width",
            reader)
        3 -> height = intAdapter.fromJson(reader) ?: throw Util.unexpectedNull("height", "height",
            reader)
        4 -> url = stringAdapter.fromJson(reader) ?: throw Util.unexpectedNull("url", "url", reader)
        5 -> download_url = stringAdapter.fromJson(reader) ?:
            throw Util.unexpectedNull("download_url", "download_url", reader)
        -1 -> {
          // Unknown name, skip it.
          reader.skipName()
          reader.skipValue()
        }
      }
    }
    reader.endObject()
    return ImageDto(
        id = id ?: throw Util.missingProperty("id", "id", reader),
        author = author ?: throw Util.missingProperty("author", "author", reader),
        width = width ?: throw Util.missingProperty("width", "width", reader),
        height = height ?: throw Util.missingProperty("height", "height", reader),
        url = url ?: throw Util.missingProperty("url", "url", reader),
        download_url = download_url ?: throw Util.missingProperty("download_url", "download_url",
            reader)
    )
  }

  override fun toJson(writer: JsonWriter, value: ImageDto?) {
    if (value == null) {
      throw NullPointerException("value was null! Wrap in .nullSafe() to write nullable values.")
    }
    writer.beginObject()
    writer.name("id")
    stringAdapter.toJson(writer, value.id)
    writer.name("author")
    stringAdapter.toJson(writer, value.author)
    writer.name("width")
    intAdapter.toJson(writer, value.width)
    writer.name("height")
    intAdapter.toJson(writer, value.height)
    writer.name("url")
    stringAdapter.toJson(writer, value.url)
    writer.name("download_url")
    stringAdapter.toJson(writer, value.download_url)
    writer.endObject()
  }
}

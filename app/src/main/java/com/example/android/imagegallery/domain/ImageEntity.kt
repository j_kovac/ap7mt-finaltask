package com.example.android.imagegallery.domain

import android.os.Parcelable
import com.example.android.imagegallery.database.ImageDbo
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ImageEntity(val id: String,
                       val author: String,
                       val width: Int,
                       val height: Int,
                       val url: String,
                       val download_url: String) : Parcelable {

    @IgnoredOnParcel
    val size: String
        get() = "$width x $height"

    @IgnoredOnParcel
    var isFavorite: Boolean = false
}

fun ImageEntity.asImageDbo(): ImageDbo {
    return ImageDbo(
            id = id,
            author = author,
            width = width,
            height = height,
            url = url,
            download_url = download_url
    )
}

fun List<ImageEntity>.asImageDbo(): List<ImageDbo> {
    return map {
        ImageDbo(
                id = it.id,
                author = it.author,
                width = it.width,
                height = it.height,
                url = it.url,
                download_url = it.download_url
        )
    }
}
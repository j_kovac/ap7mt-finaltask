package com.example.android.imagegallery.databinding;
import com.example.android.imagegallery.R;
import com.example.android.imagegallery.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentSingleBindingImpl extends FragmentSingleBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.constraintLayout_top, 7);
        sViewsWithIds.put(R.id.constraintLayout_bottom, 8);
        sViewsWithIds.put(R.id.generateImg_Label, 9);
        sViewsWithIds.put(R.id.aux_layout, 10);
        sViewsWithIds.put(R.id.generateImg_greyscale_label, 11);
        sViewsWithIds.put(R.id.button_fetch, 12);
    }
    // views
    @NonNull
    private final android.widget.ScrollView mboundView0;
    @NonNull
    private final android.widget.TextView mboundView6;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener generateImgIDandroidProgressAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of image.imageID.getValue()
            //         is image.imageID.setValue((java.lang.Integer) callbackArg_0)
            int callbackArg_0 = generateImgID.getProgress();
            // localize variables for thread safety
            // image
            com.example.android.imagegallery.viewmodels.single.SingleViewModel image = mImage;
            // image != null
            boolean imageJavaLangObjectNull = false;
            // image.imageID.getValue()
            java.lang.Integer imageImageIDGetValue = null;
            // image.imageID
            androidx.lifecycle.MutableLiveData<java.lang.Integer> imageImageID = null;
            // image.imageID != null
            boolean imageImageIDJavaLangObjectNull = false;



            imageJavaLangObjectNull = (image) != (null);
            if (imageJavaLangObjectNull) {


                imageImageID = image.getImageID();

                imageImageIDJavaLangObjectNull = (imageImageID) != (null);
                if (imageImageIDJavaLangObjectNull) {




                    imageImageID.setValue(((java.lang.Integer) (callbackArg_0)));
                }
            }
        }
    };

    public FragmentSingleBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 13, sIncludes, sViewsWithIds));
    }
    private FragmentSingleBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 3
            , (android.widget.LinearLayout) bindings[10]
            , (android.widget.Button) bindings[12]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[8]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[7]
            , (android.widget.TextView) bindings[11]
            , (android.widget.SeekBar) bindings[5]
            , (android.widget.TextView) bindings[9]
            , (android.widget.TextView) bindings[4]
            , (android.widget.ImageView) bindings[2]
            , (android.widget.TextView) bindings[3]
            , (android.widget.ProgressBar) bindings[1]
            );
        this.generateImgID.setTag(null);
        this.itemAuthor.setTag(null);
        this.itemSingleImage.setTag(null);
        this.itemSize.setTag(null);
        this.loadingSpinnerSingle.setTag(null);
        this.mboundView0 = (android.widget.ScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView6 = (android.widget.TextView) bindings[6];
        this.mboundView6.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.image == variableId) {
            setImage((com.example.android.imagegallery.viewmodels.single.SingleViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setImage(@Nullable com.example.android.imagegallery.viewmodels.single.SingleViewModel Image) {
        this.mImage = Image;
        synchronized(this) {
            mDirtyFlags |= 0x8L;
        }
        notifyPropertyChanged(BR.image);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeImageImageID((androidx.lifecycle.MutableLiveData<java.lang.Integer>) object, fieldId);
            case 1 :
                return onChangeImageEventNetworkError((androidx.lifecycle.LiveData<java.lang.Boolean>) object, fieldId);
            case 2 :
                return onChangeImageImageLoaded((androidx.lifecycle.LiveData<com.example.android.imagegallery.domain.ImageEntity>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeImageImageID(androidx.lifecycle.MutableLiveData<java.lang.Integer> ImageImageID, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeImageEventNetworkError(androidx.lifecycle.LiveData<java.lang.Boolean> ImageEventNetworkError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeImageImageLoaded(androidx.lifecycle.LiveData<com.example.android.imagegallery.domain.ImageEntity> ImageImageLoaded, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.example.android.imagegallery.viewmodels.single.SingleViewModel image = mImage;
        com.example.android.imagegallery.domain.ImageEntity imageImageLoadedGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.Integer> imageImageID = null;
        int androidxDatabindingViewDataBindingSafeUnboxImageImageIDGetValue = 0;
        java.lang.String imageImageLoadedAuthor = null;
        java.lang.Boolean imageEventNetworkErrorGetValue = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxImageEventNetworkError = false;
        androidx.lifecycle.LiveData<java.lang.Boolean> imageEventNetworkError = null;
        java.lang.String imageImageIDToString = null;
        java.lang.String imageImageLoadedDownloadUrl = null;
        java.lang.Integer imageImageIDGetValue = null;
        androidx.lifecycle.LiveData<com.example.android.imagegallery.domain.ImageEntity> imageImageLoaded = null;
        java.lang.String imageImageLoadedSize = null;

        if ((dirtyFlags & 0x1fL) != 0) {


            if ((dirtyFlags & 0x19L) != 0) {

                    if (image != null) {
                        // read image.imageID
                        imageImageID = image.getImageID();
                    }
                    updateLiveDataRegistration(0, imageImageID);


                    if (imageImageID != null) {
                        // read image.imageID.getValue()
                        imageImageIDGetValue = imageImageID.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(image.imageID.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxImageImageIDGetValue = androidx.databinding.ViewDataBinding.safeUnbox(imageImageIDGetValue);
                    if (imageImageIDGetValue != null) {
                        // read image.imageID.getValue().toString()
                        imageImageIDToString = imageImageIDGetValue.toString();
                    }
            }
            if ((dirtyFlags & 0x1eL) != 0) {

                    if (image != null) {
                        // read image.eventNetworkError
                        imageEventNetworkError = image.getEventNetworkError();
                        // read image.imageLoaded
                        imageImageLoaded = image.getImageLoaded();
                    }
                    updateLiveDataRegistration(1, imageEventNetworkError);
                    updateLiveDataRegistration(2, imageImageLoaded);


                    if (imageEventNetworkError != null) {
                        // read image.eventNetworkError.getValue()
                        imageEventNetworkErrorGetValue = imageEventNetworkError.getValue();
                    }
                    if (imageImageLoaded != null) {
                        // read image.imageLoaded.getValue()
                        imageImageLoadedGetValue = imageImageLoaded.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(image.eventNetworkError.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxImageEventNetworkError = androidx.databinding.ViewDataBinding.safeUnbox(imageEventNetworkErrorGetValue);
                if ((dirtyFlags & 0x1cL) != 0) {

                        if (imageImageLoadedGetValue != null) {
                            // read image.imageLoaded.getValue().author
                            imageImageLoadedAuthor = imageImageLoadedGetValue.getAuthor();
                            // read image.imageLoaded.getValue().download_url
                            imageImageLoadedDownloadUrl = imageImageLoadedGetValue.getDownload_url();
                            // read image.imageLoaded.getValue().size
                            imageImageLoadedSize = imageImageLoadedGetValue.getSize();
                        }
                }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x19L) != 0) {
            // api target 1

            androidx.databinding.adapters.SeekBarBindingAdapter.setProgress(this.generateImgID, androidxDatabindingViewDataBindingSafeUnboxImageImageIDGetValue);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView6, imageImageIDToString);
        }
        if ((dirtyFlags & 0x10L) != 0) {
            // api target 1

            androidx.databinding.adapters.SeekBarBindingAdapter.setOnSeekBarChangeListener(this.generateImgID, (androidx.databinding.adapters.SeekBarBindingAdapter.OnStartTrackingTouch)null, (androidx.databinding.adapters.SeekBarBindingAdapter.OnStopTrackingTouch)null, (androidx.databinding.adapters.SeekBarBindingAdapter.OnProgressChanged)null, generateImgIDandroidProgressAttrChanged);
        }
        if ((dirtyFlags & 0x1cL) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.itemAuthor, imageImageLoadedAuthor);
            com.example.android.imagegallery.util.BindingAdaptersKt.setImageUrl(this.itemSingleImage, imageImageLoadedDownloadUrl);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.itemSize, imageImageLoadedSize);
        }
        if ((dirtyFlags & 0x1eL) != 0) {
            // api target 1

            com.example.android.imagegallery.util.BindingAdaptersKt.hideIfNetworkError(this.loadingSpinnerSingle, androidxDatabindingViewDataBindingSafeUnboxImageEventNetworkError, imageImageLoadedGetValue);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): image.imageID
        flag 1 (0x2L): image.eventNetworkError
        flag 2 (0x3L): image.imageLoaded
        flag 3 (0x4L): image
        flag 4 (0x5L): null
    flag mapping end*/
    //end
}
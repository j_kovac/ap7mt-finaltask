package com.example.android.imagegallery.viewmodels.detail

import android.app.Application
import androidx.lifecycle.*
import com.example.android.imagegallery.domain.ImageEntity
import com.example.android.imagegallery.util.SingleLiveEvent

class DetailViewModel(image: ImageEntity,
                      app: Application) : AndroidViewModel(app) {

    private val _selectedImage = MutableLiveData<ImageEntity>()
    val selectedImage: LiveData<ImageEntity>
        get() = _selectedImage

    private val _navigateToPage = SingleLiveEvent<Any>()
    val navigateToPage: LiveData<Any>
        get() = _navigateToPage

    private val _saveImageToGallery = SingleLiveEvent<Any>()
    val saveImageToGallery: LiveData<Any>
        get() = _saveImageToGallery

    private val _setImageAsWallpaper = SingleLiveEvent<Any>()
    val setImageAsWallpaper: LiveData<Any>
        get() = _setImageAsWallpaper


    init {
        _selectedImage.value = image
    }

    fun navigateToImageUrl() {
        _navigateToPage.call()
    }

    fun saveImageToGallery() {
        _saveImageToGallery.call()
    }

    fun setImageAsWallpaper() {
        _setImageAsWallpaper.call()
    }
}

class DetailViewModelFactory(
        private val application: Application,
        private val selectedImage: ImageEntity? = null
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DetailViewModel::class.java) && selectedImage != null) {
            return DetailViewModel(selectedImage, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
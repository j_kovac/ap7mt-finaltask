package com.example.android.imagegallery.network;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\n \u0005*\u0004\u0018\u00010\u00070\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\b\u001a\u00020\t8FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u000e"}, d2 = {"Lcom/example/android/imagegallery/network/ImageApi;", "", "()V", "moshi", "Lcom/squareup/moshi/Moshi;", "kotlin.jvm.PlatformType", "retrofit", "Lretrofit2/Retrofit;", "retrofitUtil", "Lcom/example/android/imagegallery/network/ImageApiService;", "getRetrofitUtil", "()Lcom/example/android/imagegallery/network/ImageApiService;", "retrofitUtil$delegate", "Lkotlin/Lazy;", "app_debug"})
public final class ImageApi {
    private static final com.squareup.moshi.Moshi moshi = null;
    private static final retrofit2.Retrofit retrofit = null;
    @org.jetbrains.annotations.NotNull()
    private static final kotlin.Lazy retrofitUtil$delegate = null;
    public static final com.example.android.imagegallery.network.ImageApi INSTANCE = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.android.imagegallery.network.ImageApiService getRetrofitUtil() {
        return null;
    }
    
    private ImageApi() {
        super();
    }
}
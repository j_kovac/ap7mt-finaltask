package com.example.android.imagegallery;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.example.android.imagegallery.databinding.FavoritesItemBindingImpl;
import com.example.android.imagegallery.databinding.FragmentDetailBindingImpl;
import com.example.android.imagegallery.databinding.FragmentFavoritesBindingImpl;
import com.example.android.imagegallery.databinding.FragmentGalleryBindingImpl;
import com.example.android.imagegallery.databinding.FragmentSingleBindingImpl;
import com.example.android.imagegallery.databinding.ImagegalleryItemBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_FAVORITESITEM = 1;

  private static final int LAYOUT_FRAGMENTDETAIL = 2;

  private static final int LAYOUT_FRAGMENTFAVORITES = 3;

  private static final int LAYOUT_FRAGMENTGALLERY = 4;

  private static final int LAYOUT_FRAGMENTSINGLE = 5;

  private static final int LAYOUT_IMAGEGALLERYITEM = 6;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(6);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.android.imagegallery.R.layout.favorites_item, LAYOUT_FAVORITESITEM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.android.imagegallery.R.layout.fragment_detail, LAYOUT_FRAGMENTDETAIL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.android.imagegallery.R.layout.fragment_favorites, LAYOUT_FRAGMENTFAVORITES);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.android.imagegallery.R.layout.fragment_gallery, LAYOUT_FRAGMENTGALLERY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.android.imagegallery.R.layout.fragment_single, LAYOUT_FRAGMENTSINGLE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.android.imagegallery.R.layout.imagegallery_item, LAYOUT_IMAGEGALLERYITEM);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_FAVORITESITEM: {
          if ("layout/favorites_item_0".equals(tag)) {
            return new FavoritesItemBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for favorites_item is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTDETAIL: {
          if ("layout/fragment_detail_0".equals(tag)) {
            return new FragmentDetailBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_detail is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTFAVORITES: {
          if ("layout/fragment_favorites_0".equals(tag)) {
            return new FragmentFavoritesBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_favorites is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTGALLERY: {
          if ("layout/fragment_gallery_0".equals(tag)) {
            return new FragmentGalleryBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_gallery is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTSINGLE: {
          if ("layout/fragment_single_0".equals(tag)) {
            return new FragmentSingleBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_single is invalid. Received: " + tag);
        }
        case  LAYOUT_IMAGEGALLERYITEM: {
          if ("layout/imagegallery_item_0".equals(tag)) {
            return new ImagegalleryItemBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for imagegallery_item is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(3);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "image");
      sKeys.put(2, "viewModel");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(6);

    static {
      sKeys.put("layout/favorites_item_0", com.example.android.imagegallery.R.layout.favorites_item);
      sKeys.put("layout/fragment_detail_0", com.example.android.imagegallery.R.layout.fragment_detail);
      sKeys.put("layout/fragment_favorites_0", com.example.android.imagegallery.R.layout.fragment_favorites);
      sKeys.put("layout/fragment_gallery_0", com.example.android.imagegallery.R.layout.fragment_gallery);
      sKeys.put("layout/fragment_single_0", com.example.android.imagegallery.R.layout.fragment_single);
      sKeys.put("layout/imagegallery_item_0", com.example.android.imagegallery.R.layout.imagegallery_item);
    }
  }
}

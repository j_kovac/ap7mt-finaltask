package com.example.android.imagegallery.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\"\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0000\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u0002\u001a\u00020\u0003\u001a\u001e\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00012\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n\u00a8\u0006\u000b"}, d2 = {"extractBitmapFromImgView", "Landroid/graphics/Bitmap;", "imageView", "Landroid/widget/ImageView;", "saveMediaToStorage", "", "bitmap", "filename", "", "context", "Landroid/content/Context;", "app_debug"})
public final class UtilKt {
    
    @org.jetbrains.annotations.Nullable()
    public static final android.graphics.Bitmap extractBitmapFromImgView(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView imageView) {
        return null;
    }
    
    public static final void saveMediaToStorage(@org.jetbrains.annotations.NotNull()
    android.graphics.Bitmap bitmap, @org.jetbrains.annotations.NotNull()
    java.lang.String filename, @org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
}
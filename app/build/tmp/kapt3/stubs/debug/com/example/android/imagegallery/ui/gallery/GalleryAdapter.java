package com.example.android.imagegallery.ui.gallery;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\u001b\u001c\u001dB\u001d\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\bJ\b\u0010\u0011\u001a\u00020\u0012H\u0016J\u0018\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00022\u0006\u0010\u0016\u001a\u00020\u0012H\u0016J\u0018\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0012H\u0016R0\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\n2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"}, d2 = {"Lcom/example/android/imagegallery/ui/gallery/GalleryAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/example/android/imagegallery/ui/gallery/ImageViewHolder;", "onClickListener", "Lcom/example/android/imagegallery/ui/gallery/GalleryAdapter$OnClickListener;", "onScrollListener", "Lcom/example/android/imagegallery/ui/gallery/GalleryAdapter$OnScrollListener;", "onClickListenerFav", "(Lcom/example/android/imagegallery/ui/gallery/GalleryAdapter$OnClickListener;Lcom/example/android/imagegallery/ui/gallery/GalleryAdapter$OnScrollListener;Lcom/example/android/imagegallery/ui/gallery/GalleryAdapter$OnClickListener;)V", "value", "", "Lcom/example/android/imagegallery/domain/ImageEntity;", "images", "getImages", "()Ljava/util/List;", "setImages", "(Ljava/util/List;)V", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "OnClickListener", "OnClickListenerFav", "OnScrollListener", "app_debug"})
public final class GalleryAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.example.android.imagegallery.ui.gallery.ImageViewHolder> {
    @org.jetbrains.annotations.NotNull()
    private java.util.List<com.example.android.imagegallery.domain.ImageEntity> images;
    private final com.example.android.imagegallery.ui.gallery.GalleryAdapter.OnClickListener onClickListener = null;
    private final com.example.android.imagegallery.ui.gallery.GalleryAdapter.OnScrollListener onScrollListener = null;
    private final com.example.android.imagegallery.ui.gallery.GalleryAdapter.OnClickListener onClickListenerFav = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.android.imagegallery.domain.ImageEntity> getImages() {
        return null;
    }
    
    public final void setImages(@org.jetbrains.annotations.NotNull()
    java.util.List<com.example.android.imagegallery.domain.ImageEntity> value) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.example.android.imagegallery.ui.gallery.ImageViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.example.android.imagegallery.ui.gallery.ImageViewHolder holder, int position) {
    }
    
    public GalleryAdapter(@org.jetbrains.annotations.NotNull()
    com.example.android.imagegallery.ui.gallery.GalleryAdapter.OnClickListener onClickListener, @org.jetbrains.annotations.NotNull()
    com.example.android.imagegallery.ui.gallery.GalleryAdapter.OnScrollListener onScrollListener, @org.jetbrains.annotations.NotNull()
    com.example.android.imagegallery.ui.gallery.GalleryAdapter.OnClickListener onClickListenerFav) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B(\u0012!\u0010\u0002\u001a\u001d\u0012\u0013\u0012\u00110\u0004\u00a2\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u0003\u00a2\u0006\u0002\u0010\tJ\u000e\u0010\f\u001a\u00020\b2\u0006\u0010\u0007\u001a\u00020\u0004R,\u0010\u0002\u001a\u001d\u0012\u0013\u0012\u00110\u0004\u00a2\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\r"}, d2 = {"Lcom/example/android/imagegallery/ui/gallery/GalleryAdapter$OnClickListenerFav;", "", "clickListener", "Lkotlin/Function1;", "Lcom/example/android/imagegallery/domain/ImageEntity;", "Lkotlin/ParameterName;", "name", "image", "", "(Lkotlin/jvm/functions/Function1;)V", "getClickListener", "()Lkotlin/jvm/functions/Function1;", "onClick", "app_debug"})
    public static final class OnClickListenerFav {
        @org.jetbrains.annotations.NotNull()
        private final kotlin.jvm.functions.Function1<com.example.android.imagegallery.domain.ImageEntity, kotlin.Unit> clickListener = null;
        
        public final void onClick(@org.jetbrains.annotations.NotNull()
        com.example.android.imagegallery.domain.ImageEntity image) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final kotlin.jvm.functions.Function1<com.example.android.imagegallery.domain.ImageEntity, kotlin.Unit> getClickListener() {
            return null;
        }
        
        public OnClickListenerFav(@org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super com.example.android.imagegallery.domain.ImageEntity, kotlin.Unit> clickListener) {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B(\u0012!\u0010\u0002\u001a\u001d\u0012\u0013\u0012\u00110\u0004\u00a2\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u0003\u00a2\u0006\u0002\u0010\tJ\u000e\u0010\f\u001a\u00020\b2\u0006\u0010\u0007\u001a\u00020\u0004R,\u0010\u0002\u001a\u001d\u0012\u0013\u0012\u00110\u0004\u00a2\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\r"}, d2 = {"Lcom/example/android/imagegallery/ui/gallery/GalleryAdapter$OnClickListener;", "", "clickListener", "Lkotlin/Function1;", "Lcom/example/android/imagegallery/domain/ImageEntity;", "Lkotlin/ParameterName;", "name", "image", "", "(Lkotlin/jvm/functions/Function1;)V", "getClickListener", "()Lkotlin/jvm/functions/Function1;", "onClick", "app_debug"})
    public static final class OnClickListener {
        @org.jetbrains.annotations.NotNull()
        private final kotlin.jvm.functions.Function1<com.example.android.imagegallery.domain.ImageEntity, kotlin.Unit> clickListener = null;
        
        public final void onClick(@org.jetbrains.annotations.NotNull()
        com.example.android.imagegallery.domain.ImageEntity image) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final kotlin.jvm.functions.Function1<com.example.android.imagegallery.domain.ImageEntity, kotlin.Unit> getClickListener() {
            return null;
        }
        
        public OnClickListener(@org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super com.example.android.imagegallery.domain.ImageEntity, kotlin.Unit> clickListener) {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u0006\u0010\b\u001a\u00020\u0004R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\t"}, d2 = {"Lcom/example/android/imagegallery/ui/gallery/GalleryAdapter$OnScrollListener;", "", "scrollListener", "Lkotlin/Function0;", "", "(Lkotlin/jvm/functions/Function0;)V", "getScrollListener", "()Lkotlin/jvm/functions/Function0;", "onScroll", "app_debug"})
    public static final class OnScrollListener {
        @org.jetbrains.annotations.NotNull()
        private final kotlin.jvm.functions.Function0<kotlin.Unit> scrollListener = null;
        
        public final void onScroll() {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final kotlin.jvm.functions.Function0<kotlin.Unit> getScrollListener() {
            return null;
        }
        
        public OnScrollListener(@org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function0<kotlin.Unit> scrollListener) {
            super();
        }
    }
}
package com.example.android.imagegallery.ui.favorites;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0019\u001aB\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0006J\b\u0010\u000f\u001a\u00020\u0010H\u0016J\u0018\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u0010H\u0016J\u0018\u0010\u0015\u001a\u00020\u00022\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0010H\u0016R0\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\b2\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"}, d2 = {"Lcom/example/android/imagegallery/ui/favorites/FavoritesAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/example/android/imagegallery/ui/favorites/ImageViewHolder;", "onClickListener", "Lcom/example/android/imagegallery/ui/favorites/FavoritesAdapter$OnClickListener;", "onClickListenerDelete", "(Lcom/example/android/imagegallery/ui/favorites/FavoritesAdapter$OnClickListener;Lcom/example/android/imagegallery/ui/favorites/FavoritesAdapter$OnClickListener;)V", "value", "", "Lcom/example/android/imagegallery/domain/ImageEntity;", "images", "getImages", "()Ljava/util/List;", "setImages", "(Ljava/util/List;)V", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "OnClickListener", "OnClickListenerDelete", "app_debug"})
public final class FavoritesAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.example.android.imagegallery.ui.favorites.ImageViewHolder> {
    @org.jetbrains.annotations.NotNull()
    private java.util.List<com.example.android.imagegallery.domain.ImageEntity> images;
    private final com.example.android.imagegallery.ui.favorites.FavoritesAdapter.OnClickListener onClickListener = null;
    private final com.example.android.imagegallery.ui.favorites.FavoritesAdapter.OnClickListener onClickListenerDelete = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.android.imagegallery.domain.ImageEntity> getImages() {
        return null;
    }
    
    public final void setImages(@org.jetbrains.annotations.NotNull()
    java.util.List<com.example.android.imagegallery.domain.ImageEntity> value) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.example.android.imagegallery.ui.favorites.ImageViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.example.android.imagegallery.ui.favorites.ImageViewHolder holder, int position) {
    }
    
    public FavoritesAdapter(@org.jetbrains.annotations.NotNull()
    com.example.android.imagegallery.ui.favorites.FavoritesAdapter.OnClickListener onClickListener, @org.jetbrains.annotations.NotNull()
    com.example.android.imagegallery.ui.favorites.FavoritesAdapter.OnClickListener onClickListenerDelete) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B(\u0012!\u0010\u0002\u001a\u001d\u0012\u0013\u0012\u00110\u0004\u00a2\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u0003\u00a2\u0006\u0002\u0010\tJ\u000e\u0010\f\u001a\u00020\b2\u0006\u0010\u0007\u001a\u00020\u0004R,\u0010\u0002\u001a\u001d\u0012\u0013\u0012\u00110\u0004\u00a2\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\r"}, d2 = {"Lcom/example/android/imagegallery/ui/favorites/FavoritesAdapter$OnClickListener;", "", "clickListener", "Lkotlin/Function1;", "Lcom/example/android/imagegallery/domain/ImageEntity;", "Lkotlin/ParameterName;", "name", "image", "", "(Lkotlin/jvm/functions/Function1;)V", "getClickListener", "()Lkotlin/jvm/functions/Function1;", "onClick", "app_debug"})
    public static final class OnClickListener {
        @org.jetbrains.annotations.NotNull()
        private final kotlin.jvm.functions.Function1<com.example.android.imagegallery.domain.ImageEntity, kotlin.Unit> clickListener = null;
        
        public final void onClick(@org.jetbrains.annotations.NotNull()
        com.example.android.imagegallery.domain.ImageEntity image) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final kotlin.jvm.functions.Function1<com.example.android.imagegallery.domain.ImageEntity, kotlin.Unit> getClickListener() {
            return null;
        }
        
        public OnClickListener(@org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super com.example.android.imagegallery.domain.ImageEntity, kotlin.Unit> clickListener) {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B(\u0012!\u0010\u0002\u001a\u001d\u0012\u0013\u0012\u00110\u0004\u00a2\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u0003\u00a2\u0006\u0002\u0010\tJ\u000e\u0010\f\u001a\u00020\b2\u0006\u0010\u0007\u001a\u00020\u0004R,\u0010\u0002\u001a\u001d\u0012\u0013\u0012\u00110\u0004\u00a2\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\r"}, d2 = {"Lcom/example/android/imagegallery/ui/favorites/FavoritesAdapter$OnClickListenerDelete;", "", "clickListener", "Lkotlin/Function1;", "Lcom/example/android/imagegallery/domain/ImageEntity;", "Lkotlin/ParameterName;", "name", "image", "", "(Lkotlin/jvm/functions/Function1;)V", "getClickListener", "()Lkotlin/jvm/functions/Function1;", "onClick", "app_debug"})
    public static final class OnClickListenerDelete {
        @org.jetbrains.annotations.NotNull()
        private final kotlin.jvm.functions.Function1<com.example.android.imagegallery.domain.ImageEntity, kotlin.Unit> clickListener = null;
        
        public final void onClick(@org.jetbrains.annotations.NotNull()
        com.example.android.imagegallery.domain.ImageEntity image) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final kotlin.jvm.functions.Function1<com.example.android.imagegallery.domain.ImageEntity, kotlin.Unit> getClickListener() {
            return null;
        }
        
        public OnClickListenerDelete(@org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super com.example.android.imagegallery.domain.ImageEntity, kotlin.Unit> clickListener) {
            super();
        }
    }
}
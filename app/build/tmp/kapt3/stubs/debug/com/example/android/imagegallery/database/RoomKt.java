package com.example.android.imagegallery.database;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u001a\u0016\u0010\u0002\u001a\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"INSTANCE", "Lcom/example/android/imagegallery/database/ImagesDatabase;", "getDatabase", "context", "Landroid/content/Context;", "db", "", "app_debug"})
public final class RoomKt {
    private static com.example.android.imagegallery.database.ImagesDatabase INSTANCE;
    
    @org.jetbrains.annotations.NotNull()
    public static final com.example.android.imagegallery.database.ImagesDatabase getDatabase(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String db) {
        return null;
    }
}
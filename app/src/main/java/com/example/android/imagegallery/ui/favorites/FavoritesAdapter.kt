package com.example.android.imagegallery.ui.favorites

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.android.imagegallery.R
import com.example.android.imagegallery.databinding.FavoritesItemBinding
import com.example.android.imagegallery.domain.ImageEntity
import kotlinx.android.synthetic.main.imagegallery_item.view.*

class FavoritesAdapter(private val onClickListener: OnClickListener,
                       private val onClickListenerDelete: OnClickListener) : RecyclerView.Adapter<ImageViewHolder>() {

    var images: List<ImageEntity> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val withDataBinding: FavoritesItemBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                ImageViewHolder.LAYOUT,
                parent,
                false)
        return ImageViewHolder(withDataBinding)
    }

    override fun getItemCount() = images.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.viewDataBinding.also {
            val item = images[position]
            it.image = item

            holder.itemView.setOnClickListener {
                onClickListener.onClick(item)
            }

            holder.itemView.favorize_button.setOnClickListener {
                onClickListenerDelete.onClick(item)
            }
        }
    }

    class OnClickListener(val clickListener: (image: ImageEntity) -> Unit) {
        fun onClick(image: ImageEntity) = clickListener(image)
    }

    class OnClickListenerDelete(val clickListener: (image: ImageEntity) -> Unit) {
        fun onClick(image: ImageEntity) = clickListener(image)
    }
}

class ImageViewHolder(val viewDataBinding: FavoritesItemBinding) :
        RecyclerView.ViewHolder(viewDataBinding.root) {
    companion object {
        @LayoutRes
        val LAYOUT = R.layout.favorites_item
    }
}
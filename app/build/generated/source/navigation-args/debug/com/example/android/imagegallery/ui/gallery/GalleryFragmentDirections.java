package com.example.android.imagegallery.ui.gallery;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavDirections;
import com.example.android.imagegallery.R;
import com.example.android.imagegallery.domain.ImageEntity;
import java.io.Serializable;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class GalleryFragmentDirections {
  private GalleryFragmentDirections() {
  }

  @NonNull
  public static ActionShowDetail actionShowDetail(@Nullable ImageEntity selectedImage) {
    return new ActionShowDetail(selectedImage);
  }

  public static class ActionShowDetail implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionShowDetail(@Nullable ImageEntity selectedImage) {
      this.arguments.put("selectedImage", selectedImage);
    }

    @NonNull
    public ActionShowDetail setSelectedImage(@Nullable ImageEntity selectedImage) {
      this.arguments.put("selectedImage", selectedImage);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("selectedImage")) {
        ImageEntity selectedImage = (ImageEntity) arguments.get("selectedImage");
        if (Parcelable.class.isAssignableFrom(ImageEntity.class) || selectedImage == null) {
          __result.putParcelable("selectedImage", Parcelable.class.cast(selectedImage));
        } else if (Serializable.class.isAssignableFrom(ImageEntity.class)) {
          __result.putSerializable("selectedImage", Serializable.class.cast(selectedImage));
        } else {
          throw new UnsupportedOperationException(ImageEntity.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_showDetail;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public ImageEntity getSelectedImage() {
      return (ImageEntity) arguments.get("selectedImage");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionShowDetail that = (ActionShowDetail) object;
      if (arguments.containsKey("selectedImage") != that.arguments.containsKey("selectedImage")) {
        return false;
      }
      if (getSelectedImage() != null ? !getSelectedImage().equals(that.getSelectedImage()) : that.getSelectedImage() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getSelectedImage() != null ? getSelectedImage().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionShowDetail(actionId=" + getActionId() + "){"
          + "selectedImage=" + getSelectedImage()
          + "}";
    }
  }
}

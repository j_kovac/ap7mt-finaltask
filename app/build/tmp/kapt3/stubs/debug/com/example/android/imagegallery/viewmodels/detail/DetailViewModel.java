package com.example.android.imagegallery.viewmodels.detail;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0006\u0010\u0018\u001a\u00020\u0019J\u0006\u0010\u0012\u001a\u00020\u0019J\u0006\u0010\u0016\u001a\u00020\u0019R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00030\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\t0\u000f8F\u00a2\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011R\u0017\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\t0\u000f8F\u00a2\u0006\u0006\u001a\u0004\b\u0013\u0010\u0011R\u0017\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00030\u000f8F\u00a2\u0006\u0006\u001a\u0004\b\u0015\u0010\u0011R\u0017\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\t0\u000f8F\u00a2\u0006\u0006\u001a\u0004\b\u0017\u0010\u0011\u00a8\u0006\u001a"}, d2 = {"Lcom/example/android/imagegallery/viewmodels/detail/DetailViewModel;", "Landroidx/lifecycle/AndroidViewModel;", "image", "Lcom/example/android/imagegallery/domain/ImageEntity;", "app", "Landroid/app/Application;", "(Lcom/example/android/imagegallery/domain/ImageEntity;Landroid/app/Application;)V", "_navigateToPage", "Lcom/example/android/imagegallery/util/SingleLiveEvent;", "", "_saveImageToGallery", "_selectedImage", "Landroidx/lifecycle/MutableLiveData;", "_setImageAsWallpaper", "navigateToPage", "Landroidx/lifecycle/LiveData;", "getNavigateToPage", "()Landroidx/lifecycle/LiveData;", "saveImageToGallery", "getSaveImageToGallery", "selectedImage", "getSelectedImage", "setImageAsWallpaper", "getSetImageAsWallpaper", "navigateToImageUrl", "", "app_debug"})
public final class DetailViewModel extends androidx.lifecycle.AndroidViewModel {
    private final androidx.lifecycle.MutableLiveData<com.example.android.imagegallery.domain.ImageEntity> _selectedImage = null;
    private final com.example.android.imagegallery.util.SingleLiveEvent<java.lang.Object> _navigateToPage = null;
    private final com.example.android.imagegallery.util.SingleLiveEvent<java.lang.Object> _saveImageToGallery = null;
    private final com.example.android.imagegallery.util.SingleLiveEvent<java.lang.Object> _setImageAsWallpaper = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.example.android.imagegallery.domain.ImageEntity> getSelectedImage() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.Object> getNavigateToPage() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.Object> getSaveImageToGallery() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.Object> getSetImageAsWallpaper() {
        return null;
    }
    
    public final void navigateToImageUrl() {
    }
    
    public final void saveImageToGallery() {
    }
    
    public final void setImageAsWallpaper() {
    }
    
    public DetailViewModel(@org.jetbrains.annotations.NotNull()
    com.example.android.imagegallery.domain.ImageEntity image, @org.jetbrains.annotations.NotNull()
    android.app.Application app) {
        super(null);
    }
}
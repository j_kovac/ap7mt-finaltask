package com.example.android.imagegallery.network

import com.example.android.imagegallery.network.dto.ImageDto
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

const val URL_PREFIX = "https://picsum.photos/"

interface ImageApiService {
    @GET("v2/list")
    suspend fun getImages(@Query("page") page: Int): List<ImageDto>

    @GET("id/{id}/info")
    suspend fun getImage(@Path("id") id: Int): ImageDto
}


object ImageApi {

    private val moshi = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()

    private val retrofit = Retrofit.Builder()
            .baseUrl(URL_PREFIX)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()

    val retrofitUtil: ImageApiService by lazy { retrofit.create(ImageApiService::class.java) }
}
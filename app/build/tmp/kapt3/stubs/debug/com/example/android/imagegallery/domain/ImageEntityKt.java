package com.example.android.imagegallery.domain;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\u0016\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00010\u0003*\b\u0012\u0004\u0012\u00020\u00020\u0003\u00a8\u0006\u0004"}, d2 = {"asImageDbo", "Lcom/example/android/imagegallery/database/ImageDbo;", "Lcom/example/android/imagegallery/domain/ImageEntity;", "", "app_debug"})
public final class ImageEntityKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final com.example.android.imagegallery.database.ImageDbo asImageDbo(@org.jetbrains.annotations.NotNull()
    com.example.android.imagegallery.domain.ImageEntity $this$asImageDbo) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.util.List<com.example.android.imagegallery.database.ImageDbo> asImageDbo(@org.jetbrains.annotations.NotNull()
    java.util.List<com.example.android.imagegallery.domain.ImageEntity> $this$asImageDbo) {
        return null;
    }
}
package com.example.android.imagegallery.viewmodels.gallery


import android.accounts.NetworkErrorException
import android.app.Application
import androidx.lifecycle.*
import com.example.android.imagegallery.database.getDatabase
import com.example.android.imagegallery.domain.ImageEntity
import com.example.android.imagegallery.repository.ImagesRepository
import kotlinx.coroutines.launch
import java.io.IOException


class GalleryViewModel(application: Application) : ViewModel() {

    private var page: Int = 1

    private var _eventNetworkError = MutableLiveData<Boolean>(false)
    val eventNetworkError: LiveData<Boolean>
        get() = _eventNetworkError

    private val _navigateToSelectedImage = MutableLiveData<ImageEntity>()
    val navigateToSelectedImage: LiveData<ImageEntity>
        get() = _navigateToSelectedImage

    private val imageRepository = ImagesRepository(getDatabase(application, "lorempicsum"))

    private var imageFetchRunning = false
    val images = imageRepository.images

    init {
        loadAdditionalImages(page)
    }

    private fun loadAdditionalImages(page: Int) = viewModelScope.launch {
        viewModelScope.launch {
            try {
                _eventNetworkError.value = false
                imageRepository.getImages(page)
            } catch (networkError: IOException) {
                if (images.value.isNullOrEmpty())
                    _eventNetworkError.value = true
            }
        }
    }

    fun nextPageImages() = viewModelScope.launch {
        if (!imageFetchRunning) {
            ++page
            viewModelScope.launch {
                try {
                    _eventNetworkError.value = false

                    imageFetchRunning = true
                    imageRepository.getImages(page)
                    imageFetchRunning = false

                } catch (networkError: IOException) {
                    if (images.value.isNullOrEmpty())
                        _eventNetworkError.value = true
                }
            }
        }
    }

    fun insertFavorite(image: ImageEntity) = viewModelScope.launch {
        viewModelScope.launch {
            try {
                imageRepository.saveImage(image)
                image.isFavorite = true
            } catch (exc: NetworkErrorException) {
            }
        }
    }

    fun displayImageDetails(image: ImageEntity?) {
        _navigateToSelectedImage.value = image
    }

    fun displayImageDetailsNullifyTarget() {
        _navigateToSelectedImage.value = null
    }

    class Factory(val app: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(GalleryViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return GalleryViewModel(app) as T
            }
            throw IllegalArgumentException("Unable to construct viewmodel")
        }
    }
}
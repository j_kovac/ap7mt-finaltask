package com.example.android.imagegallery.databinding;
import com.example.android.imagegallery.R;
import com.example.android.imagegallery.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentGalleryBindingImpl extends FragmentGalleryBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.recycler_view, 2);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentGalleryBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 3, sIncludes, sViewsWithIds));
    }
    private FragmentGalleryBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 2
            , (android.widget.ProgressBar) bindings[1]
            , (androidx.recyclerview.widget.RecyclerView) bindings[2]
            );
        this.loadingSpinner.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((com.example.android.imagegallery.viewmodels.gallery.GalleryViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable com.example.android.imagegallery.viewmodels.gallery.GalleryViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelEventNetworkError((androidx.lifecycle.LiveData<java.lang.Boolean>) object, fieldId);
            case 1 :
                return onChangeViewModelImages((androidx.lifecycle.LiveData<java.util.List<com.example.android.imagegallery.domain.ImageEntity>>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelEventNetworkError(androidx.lifecycle.LiveData<java.lang.Boolean> ViewModelEventNetworkError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelImages(androidx.lifecycle.LiveData<java.util.List<com.example.android.imagegallery.domain.ImageEntity>> ViewModelImages, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelEventNetworkError = false;
        androidx.lifecycle.LiveData<java.lang.Boolean> viewModelEventNetworkError = null;
        java.lang.Boolean viewModelEventNetworkErrorGetValue = null;
        androidx.lifecycle.LiveData<java.util.List<com.example.android.imagegallery.domain.ImageEntity>> viewModelImages = null;
        com.example.android.imagegallery.viewmodels.gallery.GalleryViewModel viewModel = mViewModel;
        java.util.List<com.example.android.imagegallery.domain.ImageEntity> viewModelImagesGetValue = null;

        if ((dirtyFlags & 0xfL) != 0) {



                if (viewModel != null) {
                    // read viewModel.eventNetworkError
                    viewModelEventNetworkError = viewModel.getEventNetworkError();
                    // read viewModel.images
                    viewModelImages = viewModel.getImages();
                }
                updateLiveDataRegistration(0, viewModelEventNetworkError);
                updateLiveDataRegistration(1, viewModelImages);


                if (viewModelEventNetworkError != null) {
                    // read viewModel.eventNetworkError.getValue()
                    viewModelEventNetworkErrorGetValue = viewModelEventNetworkError.getValue();
                }
                if (viewModelImages != null) {
                    // read viewModel.images.getValue()
                    viewModelImagesGetValue = viewModelImages.getValue();
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.eventNetworkError.getValue())
                androidxDatabindingViewDataBindingSafeUnboxViewModelEventNetworkError = androidx.databinding.ViewDataBinding.safeUnbox(viewModelEventNetworkErrorGetValue);
        }
        // batch finished
        if ((dirtyFlags & 0xfL) != 0) {
            // api target 1

            com.example.android.imagegallery.util.BindingAdaptersKt.hideIfNetworkError(this.loadingSpinner, androidxDatabindingViewDataBindingSafeUnboxViewModelEventNetworkError, viewModelImagesGetValue);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.eventNetworkError
        flag 1 (0x2L): viewModel.images
        flag 2 (0x3L): viewModel
        flag 3 (0x4L): null
    flag mapping end*/
    //end
}
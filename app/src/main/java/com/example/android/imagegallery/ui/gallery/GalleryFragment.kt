package com.example.android.imagegallery.ui.gallery

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.android.imagegallery.R
import com.example.android.imagegallery.databinding.FragmentGalleryBinding
import com.example.android.imagegallery.domain.ImageEntity
import com.example.android.imagegallery.viewmodels.gallery.GalleryViewModel


class GalleryFragment : Fragment() {

    private val viewModel: GalleryViewModel by lazy {
        val activity = requireNotNull(this.activity) {
            getString(R.string.text_error_viewmodel_early_access)
        }
        ViewModelProvider(this, GalleryViewModel.Factory(activity.application))
                .get(GalleryViewModel::class.java)
    }

    private var viewModelAdapter: GalleryAdapter? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        val binding: FragmentGalleryBinding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_gallery,
                container,
                false)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        viewModelAdapter = GalleryAdapter(
                GalleryAdapter.OnClickListener { viewModel.displayImageDetails(it) },
                GalleryAdapter.OnScrollListener { viewModel.nextPageImages() },
                GalleryAdapter.OnClickListener { verifyFavorisation(it) })
        viewModel.navigateToSelectedImage.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                this.findNavController().navigate(
                        GalleryFragmentDirections.actionShowDetail(it))
                viewModel.displayImageDetails(it)
                viewModel.displayImageDetailsNullifyTarget()
            }
        })

        val recycler = binding.root.findViewById<RecyclerView>(R.id.recycler_view)
        recycler.adapter = viewModelAdapter
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.images.observe(viewLifecycleOwner, Observer<List<ImageEntity>> { img ->
            img?.apply {
                val joinedList = ArrayList<ImageEntity>()
                viewModelAdapter?.images?.let { joinedList.addAll(it) }
                joinedList.addAll(img)
                viewModelAdapter?.images = joinedList
            }
        })
    }

    private fun verifyFavorisation(image: ImageEntity) {
        if (image.isFavorite) {
            Toast.makeText(context, getString(R.string.toast_message_favorite_already_favorized), Toast.LENGTH_SHORT).show()
        } else {
            viewModel.insertFavorite(image)
            Toast.makeText(context, getString(R.string.toast_message_favorite_favorized), Toast.LENGTH_SHORT).show()
        }
    }
}
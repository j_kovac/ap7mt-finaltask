package com.example.android.imagegallery.network.dto;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\"\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u000e\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003\u001a\u001a\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u00052\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005\u001a\u0010\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0005*\u00020\t\u001a\u0010\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00010\u0005*\u00020\t\u00a8\u0006\u000b"}, d2 = {"map", "Lcom/example/android/imagegallery/domain/ImageEntity;", "it", "Lcom/example/android/imagegallery/network/dto/ImageDto;", "mapList", "", "list", "asDatabaseModel", "Lcom/example/android/imagegallery/database/ImageDbo;", "Lcom/example/android/imagegallery/network/dto/ImageContainer;", "asDomainModel", "app_debug"})
public final class ImageDtoKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final java.util.List<com.example.android.imagegallery.domain.ImageEntity> asDomainModel(@org.jetbrains.annotations.NotNull()
    com.example.android.imagegallery.network.dto.ImageContainer $this$asDomainModel) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.util.List<com.example.android.imagegallery.database.ImageDbo> asDatabaseModel(@org.jetbrains.annotations.NotNull()
    com.example.android.imagegallery.network.dto.ImageContainer $this$asDatabaseModel) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.util.List<com.example.android.imagegallery.domain.ImageEntity> mapList(@org.jetbrains.annotations.NotNull()
    java.util.List<com.example.android.imagegallery.network.dto.ImageDto> list) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final com.example.android.imagegallery.domain.ImageEntity map(@org.jetbrains.annotations.NotNull()
    com.example.android.imagegallery.network.dto.ImageDto it) {
        return null;
    }
}
package com.example.android.imagegallery.ui.single

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.android.imagegallery.R
import com.example.android.imagegallery.databinding.FragmentSingleBinding
import com.example.android.imagegallery.domain.ImageEntity
import com.example.android.imagegallery.ui.favorites.FavoritesFragmentDirections
import com.example.android.imagegallery.viewmodels.single.SingleViewModel

class SingleFragment : Fragment() {

    private val viewModel: SingleViewModel by lazy {
        val activity = requireNotNull(this.activity) {
            getString(R.string.text_error_viewmodel_early_access)
        }
        ViewModelProvider(this, SingleViewModel.Factory(activity.application))
                .get(SingleViewModel::class.java)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        val binding: FragmentSingleBinding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_single,
                container,
                false)

        val fetchButton = binding.root.findViewById<Button>(R.id.button_fetch)
        val imageDisplay = binding.root.findViewById<ImageView>(R.id.item_single_image)

        fetchButton.setOnClickListener {
            viewModel.fetchImage()
        }
        imageDisplay.setOnClickListener { viewModel.displayImageDetails() }

        viewModel.navigateToSelectedImage.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                this.findNavController().navigate(
                        FavoritesFragmentDirections.actionShowDetail(it))
                viewModel.displayImageDetails()
                viewModel.displayImageDetailsNullifyTarget()
            }
        })

        viewModel.fetchImageFromApi.observe(viewLifecycleOwner, Observer {
            viewModel.refreshRandomizedImage()
        })

        binding.lifecycleOwner = viewLifecycleOwner
        binding.image = viewModel
        return binding.root
    }

    class OnClickListener(val clickListener: (image: ImageEntity) -> Unit) {
        fun onClick(image: ImageEntity) = clickListener(image)
    }
}
package com.example.android.imagegallery.viewmodels.single

import android.app.Application
import androidx.lifecycle.*
import com.example.android.imagegallery.database.getDatabase
import com.example.android.imagegallery.domain.ImageEntity
import com.example.android.imagegallery.repository.ImagesRepository
import com.example.android.imagegallery.util.SingleLiveEvent
import kotlinx.coroutines.launch
import java.io.IOException


class SingleViewModel(application: Application) : ViewModel() {

    private val imageRepository = ImagesRepository(getDatabase(application, "lorempicsum"))

    private val _eventNetworkError = MutableLiveData<Boolean>(false)
    val eventNetworkError: LiveData<Boolean>
        get() = _eventNetworkError

    private val _fetchImageFromApi = SingleLiveEvent<Any>()
    val fetchImageFromApi: LiveData<Any>
        get() = _fetchImageFromApi

    private val _navigateToSelectedImage = MutableLiveData<ImageEntity>()
    val navigateToSelectedImage: LiveData<ImageEntity>
        get() = _navigateToSelectedImage


    var imageID = MutableLiveData(0)
    val imageLoaded = imageRepository.imageLoaded

    init {
        loadSpecifiedImage(0)
    }

    fun fetchImage() {
        _fetchImageFromApi.call()
    }

    fun refreshRandomizedImage() {
        var selectedImageID: Int? = imageID.value?.toInt()
        if (selectedImageID == null) {
            selectedImageID = 0
        }
        loadSpecifiedImage(selectedImageID)
    }

    private fun loadSpecifiedImage(id: Int) = viewModelScope.launch {
        viewModelScope.launch {
            try {
                _eventNetworkError.value = false
                imageRepository.getImageSingle(id)
            } catch (networkError: IOException) {
                if (imageLoaded.value == null)
                    _eventNetworkError.value = true
            }
        }
    }

    fun displayImageDetails() {
        val image = imageLoaded.value
        _navigateToSelectedImage.value = image
    }

    fun displayImageDetailsNullifyTarget() {
        _navigateToSelectedImage.value = null
    }


    class Factory(val app: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(SingleViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return SingleViewModel(app) as T
            }
            throw IllegalArgumentException("Unable to construct viewmodel")
        }
    }

}
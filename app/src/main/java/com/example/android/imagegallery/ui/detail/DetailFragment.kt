package com.example.android.imagegallery.ui.detail

import android.app.WallpaperManager
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.android.imagegallery.R
import com.example.android.imagegallery.databinding.FragmentDetailBinding
import com.example.android.imagegallery.domain.ImageEntity
import com.example.android.imagegallery.util.extractBitmapFromImgView
import com.example.android.imagegallery.util.saveMediaToStorage
import com.example.android.imagegallery.viewmodels.detail.DetailViewModel
import com.example.android.imagegallery.viewmodels.detail.DetailViewModelFactory
import java.io.IOException


class DetailFragment : Fragment() {
    private var viewModel: DetailViewModel? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {

        val application = requireNotNull(activity).application
        val binding: FragmentDetailBinding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_detail,
                container,
                false)

        val selectedImage: ImageEntity? = DetailFragmentArgs.fromBundle(arguments!!).selectedImage
        val viewModelFactory = DetailViewModelFactory(application, selectedImage)

        binding.lifecycleOwner = viewLifecycleOwner
        viewModel = ViewModelProvider(this, viewModelFactory
        ).get(DetailViewModel::class.java)

        val navigateToPageButton = binding.root.findViewById<Button>(R.id.button_browser)
        val setWallpaperButton = binding.root.findViewById<Button>(R.id.button_wallpaper)
        val downloadButton = binding.root.findViewById<Button>(R.id.button_save)

        if (viewModel != null) {
            navigateToPageButton.setOnClickListener {
                viewModel!!.navigateToImageUrl()
            }
            setWallpaperButton.setOnClickListener {
                viewModel!!.setImageAsWallpaper()
            }
            downloadButton.setOnClickListener {
                viewModel!!.saveImageToGallery()
            }

            viewModel!!.navigateToPage.observe(viewLifecycleOwner, Observer {
                val url = selectedImage?.url
                if (!url.isNullOrBlank()) {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(url)
                    startActivity(intent)
                }
            })

            viewModel!!.setImageAsWallpaper.observe(viewLifecycleOwner, Observer {
                val imageView = binding.root.findViewById<ImageView>(R.id.item_image)
                val bitmap = extractBitmapFromImgView(imageView)
                if (bitmap != null) {
                    val manager = WallpaperManager.getInstance(context)
                    try {
                        manager.setBitmap(bitmap)
                        Toast.makeText(context, getString(R.string.text_toast_mesage_wallpaper_set), Toast.LENGTH_SHORT).show()
                    } catch (e: IOException) {
                        Toast.makeText(context, getString(R.string.text_toast_message_wallpaper_failed), Toast.LENGTH_SHORT).show()
                    }
                } else {
                    Toast.makeText(context, getString(R.string._text_toast_message_wallpaper_failed_conversion), Toast.LENGTH_SHORT).show()
                }
            })

            viewModel!!.saveImageToGallery.observe(viewLifecycleOwner, Observer {
                val imageView = binding.root.findViewById<ImageView>(R.id.item_image)
                val bitmap = extractBitmapFromImgView(imageView)
                if (bitmap != null) {
                    val filename = "${selectedImage?.id.toString()}_${selectedImage?.author.toString()}.jpg"
                    saveMediaToStorage(bitmap, filename, context!!)
                    Toast.makeText(context, "Image saved to gallery as: $filename", Toast.LENGTH_SHORT).show()
                }
            })
        }
        binding.image = viewModel
        return binding.root
    }
}
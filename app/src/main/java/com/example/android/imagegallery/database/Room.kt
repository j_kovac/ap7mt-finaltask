package com.example.android.imagegallery.database

import android.content.Context
import androidx.room.*

@Dao
interface ImageDao {
    @Query("SELECT * FROM imagedbo")
    suspend fun getImages(): List<ImageDbo>

    @Query("DELETE FROM imagedbo WHERE [id] = :id")
    suspend fun deleteImage(id: String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertImage(image: ImageDbo)

}


@Database(entities = [ImageDbo::class], version = 1)
abstract class ImagesDatabase : RoomDatabase() {
    abstract val imageDao: ImageDao
}

private lateinit var INSTANCE: ImagesDatabase

fun getDatabase(context: Context, db: String): ImagesDatabase {
    synchronized(ImagesDatabase::class.java) {
        if (!::INSTANCE.isInitialized) {
            INSTANCE = Room.databaseBuilder(context.applicationContext,
                    ImagesDatabase::class.java, db).build()
        }
    }
    return INSTANCE
}

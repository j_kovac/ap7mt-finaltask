package com.example.android.imagegallery.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.android.imagegallery.domain.ImageEntity


@Entity
data class ImageDbo constructor(
        @PrimaryKey
        val id: String,
        val author: String,
        val width: Int,
        val height: Int,
        val url: String,
        val download_url: String)


fun List<ImageDbo>.asDomainModel(): List<ImageEntity> {
    return map {
        ImageEntity(
                id = it.id,
                author = it.author,
                width = it.width,
                height = it.height,
                url = it.url,
                download_url = it.download_url
        )
    }
}

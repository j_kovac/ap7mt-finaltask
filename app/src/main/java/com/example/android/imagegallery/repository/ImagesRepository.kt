package com.example.android.imagegallery.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.android.imagegallery.database.ImagesDatabase
import com.example.android.imagegallery.database.asDomainModel
import com.example.android.imagegallery.domain.ImageEntity
import com.example.android.imagegallery.domain.asImageDbo
import com.example.android.imagegallery.network.ImageApi
import com.example.android.imagegallery.network.dto.map
import com.example.android.imagegallery.network.dto.mapList
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ImagesRepository(private val database: ImagesDatabase) {

    private val _images = MutableLiveData<List<ImageEntity>>()
    val images: LiveData<List<ImageEntity>>
        get() = _images

    private val _imageLoaded = MutableLiveData<ImageEntity>()
    val imageLoaded: LiveData<ImageEntity>
        get() = _imageLoaded


    suspend fun getImageSingle(id: Int) {
        withContext(Dispatchers.IO) {
            try{
                val image: ImageEntity = map(ImageApi.retrofitUtil.getImage(id))
                _imageLoaded.postValue(image)
            }
            catch(ex : Exception){}
        }
    }

    suspend fun getImages(page: Int) {
        withContext(Dispatchers.IO) {
            val images: List<ImageEntity> = mapList(ImageApi.retrofitUtil.getImages(page))
            getImagesFavoriteState(images)
            _images.postValue(images)
        }
    }

    suspend fun getImagesDB() {
        withContext(Dispatchers.IO) {
            val images = database.imageDao.getImages().asDomainModel()
            _images.postValue(images)
        }
    }

    suspend fun saveImage(image: ImageEntity) {
        withContext(Dispatchers.IO) {
            database.imageDao.insertImage(image.asImageDbo())
        }
    }

    suspend fun removeImage(id: String) {
        withContext(Dispatchers.IO)
        {
            database.imageDao.deleteImage(id)
        }
    }

    suspend fun getImagesFavoriteState(imagesToAssign: List<ImageEntity>) {
        val dbImages = database.imageDao.getImages().asDomainModel().map { it.id }
        for (image in imagesToAssign) {
            image.isFavorite = dbImages.contains(image.id)
        }
    }
}

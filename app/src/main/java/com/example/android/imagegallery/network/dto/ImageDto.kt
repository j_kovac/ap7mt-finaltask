package com.example.android.imagegallery.network.dto

import com.example.android.imagegallery.database.ImageDbo
import com.example.android.imagegallery.domain.ImageEntity
import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class ImageContainer(val images: List<ImageDto>)

@JsonClass(generateAdapter = true)
data class ImageDto(
        val id: String,
        val author: String,
        val width: Int,
        val height: Int,
        val url: String,
        val download_url: String)


fun ImageContainer.asDomainModel(): List<ImageEntity> {
    return images.map {
        ImageEntity(
                id = it.id,
                author = it.author,
                width = it.width,
                height = it.height,
                url = it.url,
                download_url = it.download_url
        )
    }
}


fun ImageContainer.asDatabaseModel(): List<ImageDbo> {
    return images.map {
        ImageDbo(
                id = it.id,
                author = it.author,
                width = it.width,
                height = it.height,
                url = it.url,
                download_url = it.download_url
        )
    }
}

fun mapList(list: List<ImageDto>): List<ImageEntity> {
    return list.map {
        ImageEntity(
                id = it.id,
                author = it.author,
                width = it.width,
                height = it.height,
                url = it.url,
                download_url = it.download_url
        )
    }
}

fun map(it: ImageDto): ImageEntity {
    return ImageEntity(
            id = it.id,
            author = it.author,
            width = it.width,
            height = it.height,
            url = it.url,
            download_url = it.download_url
    )
}

